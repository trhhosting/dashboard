Member Panel

Ingress:

    1. Member must pick closest location. So deployment of ingress point on Linode or other provider 
    2. Control access point of the ingress.
    3. Total bandwidth used
    4. Ingress to be restarted: Gobetween
    5. Ingress point creation member.proxyips.cloud allow for change of member
    6. Number of ingress points
    
Mobile Proxies

    1. Bandwidth usage
    2. Creation of ingress points
    3. Dedicated modems only
        ◦ SMS sending and reciving
        ◦ Set modem rotation interval for IP Address changes
        ◦ API access to the above
    4. Modem walk 
        ◦ City to City travel of modem 
        ◦ Point to point travel on interstate to get changing IP and regions
    5. Show number of modems
    6. Grouping of modems for endpoints fail-over
    7. Creation of endpoints for modem groups
    8. Creation of access control for endpoints
    9. Set modem rotation interval for IP Address changes

Residential Proxies

    1. Bandwidth usage
    2. Creation of ingress points
    3. Control ingress point access
    4. Buy additional bandwidth

Datacenter Proxies

    1. Bandwidth usage
    2. Creation of ingress points
    3. Control ingress point access
    4. Create groups of IPs for fail-over
    5. Provide IP Address
        ◦ Static Address
        ◦ Rotating Address 