import 'dart:html';

import 'package:htmlcomp/src/validators.dart';

class _ItemUrlPolicy implements UriPolicy {
  RegExp regex = RegExp(r'(?:https://)?.*');
  @override
  bool allowsUri(String uri) {
    return regex.hasMatch(uri);
  }
}
class CustomHtmlValidator {
    NodeValidatorBuilder validator = NodeValidatorBuilder()
  ..allowTextElements()
  ..allowTemplating()
  ..allowHtml5(uriPolicy: _ItemUrlPolicy())
  ..allowNavigation(_ItemUrlPolicy())
  ..allowImages(_ItemUrlPolicy())
  ..allowElement('a',attributes: ['onclick', 'data-toggle', 'aria-expanded'])
  ..allowElement('input',attributes: ['style'])
  ..allowElement('div',attributes: ['style','onclick','role'])
  ..allowElement('button',attributes: ['style','onclick','aria-expanded','data-toggle'])
  ..allowElement('ul',attributes: ['style','onclick','aria-expanded','data-toggle', 'role'])
  ..allowElement('textarea',attributes: ['row','style','onclick'])
  ..allowElement('i',attributes: ['aria-hidden','row','style','onclick'])
  ..allowSvg();
  GodNodeValidator _vali = GodNodeValidator();
  CustomHtmlValidator();
  NodeValidatorBuilder get v => validator;

  //vali accept everything
  GodNodeValidator get vali => this._vali;
  set vali(GodNodeValidator vali){
  this._vali = vali;
  }
}
