class RegisterAccount {
  String _email;
  String _btcAddress;
  String _passwordOne;
  String _passwordTwo;
  String _userName;

  RegisterAccount(
      {String email,
      String btcAddress,
      String passwordOne,
      String passwordTwo,
      String userName}) {
    this._email = email;
    this._btcAddress = btcAddress;
    this._passwordOne = passwordOne;
    this._passwordTwo = passwordTwo;
    this._userName = userName;
  }

  String get email => _email;
  set email(String email) => _email = email;
  String get btcAddress => _btcAddress;
  set btcAddress(String btcAddress) => _btcAddress = btcAddress;
  String get passwordOne => _passwordOne;
  set passwordOne(String passwordOne) => _passwordOne = passwordOne;
  String get passwordTwo => _passwordTwo;
  set passwordTwo(String passwordTwo) => _passwordTwo = passwordTwo;
  String get userName => _userName;
  set userName(String userName) => _userName = userName;

  RegisterAccount.fromJson(Map<String, dynamic> json) {
    _email = json['email'];
    _btcAddress = json['btc_address'];
    _passwordOne = json['password_one'];
    _passwordTwo = json['password_two'];
    _userName = json['user_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = this._email;
    data['btc_address'] = this._btcAddress;
    data['password_one'] = this._passwordOne;
    data['password_two'] = this._passwordTwo;
    data['user_name'] = this._userName;
    return data;
  }
}
class MyEvent {
  String _name;
  String _action;

  MyEvent({String name, String action}) {
    this._name = name;
    this._action = action;
  }

  String get name => _name;
  set name(String name) => _name = name;
  String get action => _action;
  set action(String action) => _action = action;

  MyEvent.fromJson(Map<String, dynamic> json) {
    _name = json['name'];
    _action = json['action'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = this._name;
    data['action'] = this._action;
    return data;
  }
}
class UserLogin {
  String _userName;
  String _password;

  UserLogin({String userName, String password}) {
    this._userName = userName;
    this._password = password;
  }

  String get userName => _userName;
  set userName(String userName) => _userName = userName;
  String get password => _password;
  set password(String password) => _password = password;

  UserLogin.fromJson(Map<String, dynamic> json) {
    _userName = json['user_name'];
    _password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_name'] = this._userName;
    data['password'] = this._password;
    return data;
  }
}
