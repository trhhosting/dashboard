String accountPanel(num total, String name){
  return '''
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="info-box bg-primary">
                            <div class="icon-box">
                                <i class="lni-home"></i>
                            </div>
                            <div class="info-box-content">
                                <h4 class="number">1125</h4>
                                <p class="info-text">All Properties</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="info-box bg-success">
                            <div class="icon-box">
                                <i class="lni-tag"></i>
                            </div>
                            <div class="info-box-content">
                                <h4 class="number">351</h4>
                                <p class="info-text">For Sale</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="info-box bg-info">
                            <div class="icon-box">
                                <i class="lni-cart"></i>
                            </div>
                            <div class="info-box-content">
                                <h4 class="number">774</h4>
                                <p class="info-text">For Rent</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="info-box bg-purple">
                            <div class="icon-box">
                                <i class="lni-wallet"></i>
                            </div>
                            <div class="info-box-content">
                                <h4 class="number">\$${total}</h4>
                                <p class="info-text">Total Revenue</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-12 col-xs-12">
                      <div class="row">
                        <!-- User Summary -->
                        <div class="col-12 col-md-4">
                          <div class="profile-bg bg-dark">
                            <div class="user-profile">
                              <figure class="profile-wall-img">
                                <img class="img-fluid" src="assets/img/profile/user-bg.jpg" alt="User Wall">
                              </figure>
                              <div class="profile-body">
                                <figure class="profile-user-avatar">
                                  <img src="assets/img/profile/user1.jpg" alt="User Wall">
                                </figure>
                                <h3 class="profile-user-name text-white">Michael A. Franklin</h3>
                                <small class="profile-user-address text-white">California, United States</small>
                                <div class="profile-user-description">
                                  <p class="text-white">I have 10 years of experience designing for the web, and specialize in the areas of user interface design, interaction design, visual design and prototyping. I’ve worked with notable startups including Pearl Street Software.</p>
                                </div>
                                <div class="m-t-5">
                                  <a href="#" class="btn btn-common">Edit Profile</a> 
                                </div>
                              </div>
                              <div class="row border-top m-t-20">
                                <div class="col-4 border-right d-flex flex-column justify-content-center align-items-center py-4">
                                  <h3 class="text-white">274</h3>
                                  <small class="text-white">Comments</small>
                                </div>
                                <div class="col-4 border-right d-flex flex-column justify-content-center align-items-center py-4">
                                  <h3 class="text-white">2,483</h3>
                                  <small class="text-white">Followers</small>
                                </div>
                                <div class="col-4 border-right d-flex flex-column justify-content-center align-items-center py-4">
                                  <h3 class="text-white">146</h3>
                                  <small class="text-white">Following</small>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- Profile holder -->
                        <div class="col-12 col-md-8">
                          <div class="timeline-view bg-dark p-20">
                            <h4 class="box-title">User Timeline</h4>
                            <div class="row">
                              <div class="col-12">
                                <form class="form">
                                  <div class="form-group floating-label">
                                    <textarea class="form-control form-control" row="7" placeholder="What's on your mind"></textarea>
                                  </div>
                                </form>
                                <div class="m-b-20">
                                  <div class="float-left">
                                    <a class="btn bg-transparent"><i class="lni-camera" aria-hidden="true"></i></a>
                                    <a class="btn bg-transparent"><i class="lni-map-marker" aria-hidden="true"></i></a>
                                    <a class="btn bg-transparent"><i class="lni-paperclip" aria-hidden="true"></i></a>
                                  </div>
                                  <a href="#" class="btn btn-common float-right">Post</a>
                                </div>
                              </div>
                              <div class="col-12">
                                <div id="activity" class="m-t-20">
                                  <ul class="timeline timeline-hairline">
                                    <li class="timeline-inverted">
                                      <div class="timeline-circle rounded-circle text-primary text-center"><i class="lni-envelope"></i></div>
                                      <div class="timeline-entry">
                                        <div class="card">
                                          <div class="card-body timeline-entry-content">
                                            <p class="mb-0">Received a <a class="text-primary" href="#">message</a> from <span class="text-primary">Samuel Nelson</span></p>
                                            <p class="mb-0">
                                              <span>
                                                Sunday, March 25, 2018
                                              </span>
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="timeline-circle rounded-circle text-warning text-center"><i class="lni-alarm-clock"></i></div>
                                      <div class="timeline-entry">
                                        <div class="card">
                                          <div class="card-body timeline-entry-content">
                                            <p class="mb-0">
                                              User apply for refund at <span class="text-warning">9:15 pm</span>
                                            </p>
                                            <p class="mb-0">
                                              <span>
                                                Thursday, March 15, 2018
                                              </span>
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </li>
                                    <li>
                                      <div class="timeline-circle rounded-circle text-success text-center"><i class="lni-map-marker"></i></div>
                                      <div class="timeline-entry">
                                        <div class="card">
                                          <div class="card-body timeline-entry-content">
                                            <img class="rounded-circle float-left" src="assets/img/profile/avatar.jpg" alt="Profile Image">
                                            <div class="m-l-50">
                                              <p class="mb-0">User receives order in the <span class="text-success">Office Place</span></p>
                                              <p class="mb-0">
                                                <span>
                                                  Monday, March 5, 2018
                                                </span>
                                              </p>
                                            </div>
                                            <div class="mt-2">
                                              <p>Thank you for a good service.</p>
                                              <div class="text-center">
                                                <img class="img-fluid" src="assets/img/profile/shopping-bag.png" alt="Shopping Bag">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
            
                    <div class="col-12 col-lg-4 col-xs-12">
                        <div class="card bg-dark">
                          <h3 class="card-header text-white text-center">
                          ${name.toUpperCase()}
                          </h3>
                          <div class="card-body">
                          <form class="form">
                            <div class="form-row">
                              <div class="form-check col-md-12 m-b-20">
                                <div class="custom-control custom-radio radio custom-control-inline">
                                  <input type="radio" class="custom-control-input" name="gender" id="male1" checked="">
                                  <label class="custom-control-label text-white" for="male1">Male</label>
                                </div>
                                <div class="custom-control custom-radio radio custom-control-inline">
                                  <input type="radio" class="custom-control-input" name="gender" id="female1" checked="">
                                  <label class="custom-control-label text-white" for="female1">Female</label>
                                </div>
                              </div>
                              <div class="form-group col-lg-6 floating-label">
                                <input type="text" class="form-control" id="firstname1">
                                <label class="text-white" for="firstname1">Firstname</label>
                              </div>
                              <div class="form-group col-lg-6 floating-label">
                                <input type="text" class="form-control" id="lastname1">
                                <label class="text-white" for="lastname1">Lastname</label>
                              </div>
                              <div class="form-group col-lg-12 floating-label">
                                <input type="text" class="form-control" id="username1">
                                <label class="text-white" for="username1">Username</label>
                              </div>
                              <div class="form-group col-lg-12 floating-label">
                                <input type="text" class="form-control" id="email1">
                                <label class="text-white" for="email1">Email</label>
                              </div>
                              <div class="form-group col-lg-6 floating-label">
                                <input type="password" class="form-control" id="password1">
                                <label class="text-white" for="password1">Password</label>
                              </div>
                              <div class="form-group col-lg-6 floating-label">
                                <input type="password" class="form-control" id="confirmPassword1">
                                <label class="text-white" for="confirmPassword1">Confirm Password</label>
                              </div>
                              <div class="form-check col-md-12">
                                <div class="custom-control custom-checkbox m-b-20">
                                  <input type="checkbox" class="custom-control-input" id="weeklyUpdates">
                                  <label class="custom-control-label text-white" for="weeklyUpdates">Send me weekly updates</label>
                                </div>
                              </div>
                              <div class="form-group col-lg-12 text-righ">
                                <button type="submit" class="btn btn-common">Submit</button>
                              </div>
                            </div>
                          </form>
                          </div>
                          
                        </div>
                    </div>
                </div>
                <div class="row">
                <br>
                  <div class="col-12 col-lg-12 col-xs-12">
                  <div class="card bg-dark">
                    <div class="card-header border-bottom">
                      <h4 class="card-title text-white">Contextual tables</h4>
                    </div>
                    <div class="card-body">
                      <p class="text-white m-b-20 box-content">
                        Use contextual classes to color table rows or individual cells.
                      </p>
                      <div class="table-responsive">
                        <table class="table mb-0">
                          <thead>
                            <tr>
                              <th>Type</th>
                              <th>Column heading</th>
                              <th>Column heading</th>
                              <th>Column heading</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="table-active">
                              <th scope="row">Active</th>
                              <td>Column content</td>
                              <td>Column content</td>
                              <td>Column content</td>
                            </tr>
                            <tr>
                              <th scope="row">Default</th>
                              <td>Column content</td>
                              <td>Column content</td>
                              <td>Column content</td>
                            </tr>
                            <tr class="table-primary">
                              <th scope="row">Primary</th>
                              <td>Column content</td>
                              <td>Column content</td>
                              <td>Column content</td>
                            </tr>
                            <tr class="table-secondary">
                              <th scope="row">Secondary</th>
                              <td>Column content</td>
                              <td>Column content</td>
                              <td>Column content</td>
                            </tr>
                            <tr class="table-success">
                              <th scope="row">Success</th>
                              <td>Column content</td>
                              <td>Column content</td>
                              <td>Column content</td>
                            </tr>
                            <tr class="table-danger">
                              <th scope="row">Danger</th>
                              <td>Column content</td>
                              <td>Column content</td>
                              <td>Column content</td>
                            </tr>
                            <tr class="table-warning">
                              <th scope="row">Warning</th>
                              <td>Column content</td>
                              <td>Column content</td>
                              <td>Column content</td>
                            </tr>
                            <tr class="table-info">
                              <th scope="row">Info</th>
                              <td>Column content</td>
                              <td>Column content</td>
                              <td>Column content</td>
                            </tr>
                            <tr class="table-light">
                              <th scope="row">Light</th>
                              <td>Column content</td>
                              <td>Column content</td>
                              <td>Column content</td>
                            </tr>
                            <tr class="table-dark">
                              <th scope="row">Dark</th>
                              <td>Column content</td>
                              <td>Column content</td>
                              <td>Column content</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

                </div>
    ''';
}
