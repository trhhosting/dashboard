String notificationPanel(num total, String name){
  return '''
              <div class="container-fluid">
              <div class="row">
                <div class="col-12">
                  <div class="card bg-dark">
                    <div class="chat-main-box">
                      <div class="chat-left-aside">
                        <div class="chat-left-inner">
                          <div class="form-material">
                            <input class="form-control" type="text" placeholder="Search Contact">
                          </div>
                          <ul class="chatonline">
                            <li>
                              <a href="#">
                                <img src="assets/img/users/1.jpg" alt="user-img" class="img-circle"> 
                                <span>Richard Cook <small class="text-success">online</small></span>
                              </a>
                            </li>
                            <li>
                              <a href="#" class="active">
                                <img src="assets/img/users/2.jpg" alt="user-img" class="img-circle"> 
                                <span>Samuel Nelson <small class="text-warning">Away</small></span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <img src="assets/img/users/3.jpg" alt="user-img" class="img-circle"> 
                                <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <img src="assets/img/users/4.jpg" alt="user-img" class="img-circle"> 
                                <span>Marshall Nichols <small class="text-muted">Offline</small></span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <img src="assets/img/users/5.jpg" alt="user-img" class="img-circle"> 
                                <span>James Anderson <small class="text-success">online</small></span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <img src="assets/img/users/6.jpg" alt="user-img" class="img-circle"> 
                                <span>Susie Willis <small class="text-success"> online</small></span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <img src="assets/img/users/7.jpg" alt="user-img" class="img-circle"> 
                                <span>Debra Stewart <small class="text-success">online</small></span>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <img src="assets/img/users/8.jpg" alt="user-img" class="img-circle"> 
                                <span>Susie Willis <small class="text-success">online</small></span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>

                      <div class="chat-right-aside">
                        <div class="chat-main-header">
                          <div class="p-3 border-bottom">
                            <h4 class="box-title">Chat Message</h4>
                          </div>
                        </div>
                        <div class="chat-rbox">
                          <ul class="chat-list p-3">
                            <li>
                              <div class="chat-img"><img src="assets/img/users/1.jpg" alt="user"></div>
                              <div class="chat-content">
                                <h5>James Anderson</h5>
                                <div class="box bg-light-info">Lorem Ipsum is simply dummy text of the printing &amp; type setting industry.</div>
                                <div class="chat-time">10:56 am</div>
                              </div>
                            </li>
                            <li>
                              <div class="chat-img"><img src="assets/img/users/2.jpg" alt="user"></div>
                              <div class="chat-content">
                                <h5>Bianca Doe</h5>
                                <div class="box bg-light-info">It’s Great opportunity to work.</div>
                                <div class="chat-time">10:57 am</div>
                              </div>
                            </li>
                            <li class="reverse">
                              <div class="chat-content">
                                <h5>Steave Doe</h5>
                                <div class="box bg-light-inverse">It’s Great opportunity to work.</div>
                                <div class="chat-time">10:57 am</div>
                              </div>
                              <div class="chat-img"><img src="assets/img/users/5.jpg" alt="user"></div>
                            </li>
                            <li class="reverse">
                              <div class="chat-content">
                                <h5>Steave Doe</h5>
                                <div class="box bg-light-inverse">It’s Great opportunity to work.</div>
                                <div class="chat-time">10:57 am</div>
                              </div>
                              <div class="chat-img"><img src="assets/img/users/5.jpg" alt="user"></div>
                            </li>
                          </ul>
                          <div class="scrollbar-x-rail">
                            <div class="scrollbar-x"></div>
                          </div>
                          <div class="scrollbar-y-rail">
                            <div class="scrollbar-y"></div>
                          </div>
                        </div>
                        <div class="card-body border-top">
                          <div class="row">
                            <div class="col-8">
                              <textarea placeholder="Type your message here" class="form-control border-0"></textarea>
                            </div>
                            <div class="col-4 text-right">
                              <button type="button" class="btn btn-common btn-circle btn-lg"><i class="lni-pointer"></i> </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

''';
}
