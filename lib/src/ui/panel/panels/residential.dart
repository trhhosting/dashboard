String residentialPanel(num total, String name){
  return '''
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="info-box bg-primary">
                            <div class="icon-box">
                                <i class="lni-home"></i>
                            </div>
                            <div class="info-box-content">
                                <h4 class="number">1125</h4>
                                <p class="info-text">All Properties</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="info-box bg-success">
                            <div class="icon-box">
                                <i class="lni-tag"></i>
                            </div>
                            <div class="info-box-content">
                                <h4 class="number">351</h4>
                                <p class="info-text">For Sale</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="info-box bg-info">
                            <div class="icon-box">
                                <i class="lni-cart"></i>
                            </div>
                            <div class="info-box-content">
                                <h4 class="number">774</h4>
                                <p class="info-text">For Rent</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="info-box bg-purple">
                            <div class="icon-box">
                                <i class="lni-wallet"></i>
                            </div>
                            <div class="info-box-content">
                                <h4 class="number">\$${total}</h4>
                                <p class="info-text">Total Revenue</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-xs-12">
                                <div class="card bg-dark">
                                    <div class="card-body">
                                        <h5 class="text-white card-title">Sales Details of Properties</h5>
                                        <div class="row">
                                            <div class="col-5 m-t-20">
                                                <h3 class="text-primary">\$7492</h3>
                                                <p class="text-white text-white">July 2018</p>
                                                <b class="text-white">(149 Sales)</b>
                                            </div>
                                            <div class="col-7">
                                                <div id="sales1" class="text-right">
                                                    <div id="morris-bar-example" style="height: 130px"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-xs-12">
                                <div class="card bg-dark">
                                    <div class="card-body">
                                        <h5 class="text-white card-title">Rent Details of Properties</h5>
                                        <div class="row">
                                            <div class="col-5  m-t-20">
                                                <h3 class="text-white text-info">\$1930</h3>
                                                <p class="text-white light_op_text">July 2018</p>
                                                <b class="text-white">(170 Rentals)</b>
                                            </div>
                                            <div class="col-7">
                                                <div id="morris-bar-stacked" style="height: 130px"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="properties">
                                    <div class="card bg-dark">
                                        <div class="card-header">
                                            <h4 class="text-white card-title">Recent properties</h4>
                                            <div class="card-toolbar">
                                                <ul>
                                                    <li>
                                                        <a class="text-white" href="#">
                                                            <i class="lni-more-alt"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <ul class="list-media">
                                                <li class="list-item">
                                                    <div class="client-item">
                                                        <div class="media-img">
                                                            <img src="assets/img/properties/img1.jpg" alt="">
                                                        </div>
                                                        <div class="info">
                                                            <h4 class="title text-semibold"><a href="#">795 Folsom Ave, Suite 600
                                                                    San Francisco</a></h4>
                                                            <p class="m-0">20 July 2018, John Doe</p>
                                                            <div class="float-item">
                                                                <button class="btn btn-success">Rent</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="list-item">
                                                    <div class="client-item">
                                                        <div class="media-img">
                                                            <img src="assets/img/properties/img2.jpg" alt="">
                                                        </div>
                                                        <div class="info">
                                                            <h4 class="title text-semibold"><a href="#">795 Folsom Ave, Suite 600
                                                                    San Francisco</a></h4>
                                                            <p class="m-0">17 July 2018, Venessa Fern</p>
                                                            <div class="float-item">
                                                                <button class="btn btn-common">Sale</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="list-item">
                                                    <div class="client-item">
                                                        <div class="media-img">
                                                            <img src="assets/img/properties/img3.jpg" alt="">
                                                        </div>
                                                        <div class="info">
                                                            <h4 class="title text-semibold"><a href="#">795 Folsom Ave, Suite 600
                                                                    San Francisco</a></h4>
                                                            <p class="m-0">13 July 2018, Danielle M. Stong</p>
                                                            <div class="float-item">
                                                                <button class="btn btn-success">Rent</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="list-item">
                                                    <div class="client-item">
                                                        <div class="media-img">
                                                            <img src="assets/img/properties/img4.jpg" alt="">
                                                        </div>
                                                        <div class="info">
                                                            <h4 class="title text-semibold"><a href="#">795 Folsom Ave, Suite 600
                                                                    San Francisco</a></h4>
                                                            <p class="m-0">09 July 2018, Ample</p>
                                                            <div class="float-item">
                                                                <button class="btn btn-common">Sale</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                                      <div class="card bg-dark">
                    <div class="card-header border-bottom">
                      <h4 class="card-title text-white">Inline forms</h4>
                    </div>
                    <div class="card-body">
                      <p class="card-description text-white">
                        Use the <code>.form-inline</code> class to display a series of labels, form controls, and buttons on a single horizontal row
                      </p>
                      <form class="form-inline">
                        <label class="sr-only text-white" for="inlineFormInputName2">Name</label>
                        <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Jane Doe">

                        <label class="sr-only text-white" for="inlineFormInputGroupUsername2">Username</label>
                        <div class="input-group mb-2 mr-sm-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text">@</div>
                          </div>
                          <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Username">
                        </div>
                        <div class="form-check mx-sm-3">
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="checkbox5" value="check">
                            <label class="custom-control-label text-white" for="checkbox5">Remember me</label>
                          </div>
                        </div>
                        <button type="submit" class="btn btn-common mb-3">Submit</button>
                      </form>
                    </div>
                  </div>

                                </div>
                            </div>
                        </div>
                    </div>
            
                    <div class="col-12 col-lg-4 col-xs-12">
                        <div class="card bg-dark">
                          <h3 class="card-header text-white text-center">
                          ${name.toUpperCase()}
                          </h3>
                          <div class="card-body">
                          <form class="form">
                            <div class="form-row">
                              <div class="form-check col-md-12 m-b-20">
                                <div class="custom-control custom-radio radio custom-control-inline">
                                  <input type="radio" class="custom-control-input" name="gender" id="male1" checked="">
                                  <label class="custom-control-label text-white" for="male1">Male</label>
                                </div>
                                <div class="custom-control custom-radio radio custom-control-inline">
                                  <input type="radio" class="custom-control-input" name="gender" id="female1" checked="">
                                  <label class="custom-control-label text-white" for="female1">Female</label>
                                </div>
                              </div>
                              <div class="form-group col-lg-6 floating-label">
                                <input type="text" class="form-control" id="firstname1">
                                <label class="text-white" for="firstname1">Firstname</label>
                              </div>
                              <div class="form-group col-lg-6 floating-label">
                                <input type="text" class="form-control" id="lastname1">
                                <label class="text-white" for="lastname1">Lastname</label>
                              </div>
                              <div class="form-group col-lg-12 floating-label">
                                <input type="text" class="form-control" id="username1">
                                <label class="text-white" for="username1">Username</label>
                              </div>
                              <div class="form-group col-lg-12 floating-label">
                                <input type="text" class="form-control" id="email1">
                                <label class="text-white" for="email1">Email</label>
                              </div>
                              <div class="form-group col-lg-6 floating-label">
                                <input type="password" class="form-control" id="password1">
                                <label class="text-white" for="password1">Password</label>
                              </div>
                              <div class="form-group col-lg-6 floating-label">
                                <input type="password" class="form-control" id="confirmPassword1">
                                <label class="text-white" for="confirmPassword1">Confirm Password</label>
                              </div>
                              <div class="form-check col-md-12">
                                <div class="custom-control custom-checkbox m-b-20">
                                  <input type="checkbox" class="custom-control-input" id="weeklyUpdates">
                                  <label class="custom-control-label text-white" for="weeklyUpdates">Send me weekly updates</label>
                                </div>
                              </div>
                              <div class="form-group col-lg-12 text-righ">
                                <button type="submit" class="btn btn-common">Submit</button>
                              </div>
                            </div>
                          </form>
                          </div>
                          
                        </div>
                    </div>
                </div>

    ''';
}
