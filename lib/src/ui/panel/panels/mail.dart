String mailPanel(num total, String name){
  return '''
            <div class="container-fluid">
              <div class="row">
                <!-- Left sidebar -->
                <div class="col-lg-3">
                  <a href="email-compose.html" class="btn btn-common btn-block">Compose</a>
                  <div class="list-group mail-list m-t-20">
                    <a href="#" class="list-group-item text-danger"><i class="lni-inbox m-r-5"></i>Inbox <b>(8)</b></a>
                    <a href="#" class="list-group-item"><i class="lni-star m-r-5"></i>Starred</a>
                    <a href="#" class="list-group-item"><i class="lni-book m-r-5"></i>Draft <b>(20)</b></a>
                    <a href="#" class="list-group-item"><i class="lni-pointer m-r-5"></i>Sent Mail</a>
                    <a href="#" class="list-group-item"><i class="lni-trash m-r-5"></i>Trash <b>(354)</b></a>
                  </div>
                  <h4 class="m-t-20 text-white">Labels</h4>
                  <div class="list-group m-t-20 mail-list">
                    <a href="#" class="list-group-item"><span class="lni-check-mark-circle text-info float-right"></span>Web App</a>
                    <a href="#" class="list-group-item"><span class="lni-check-mark-circle text-warning float-right"></span>Project 1</a>
                    <a href="#" class="list-group-item"><span class="lni-check-mark-circle text-purple float-right"></span>Project 2</a>
                    <a href="#" class="list-group-item"><span class="lni-check-mark-circle text-pink float-right"></span>Friends</a>
                    <a href="#" class="list-group-item"><span class="lni-check-mark-circle text-success float-right"></span>Family</a>
                  </div>

                </div>
                <!-- End Left sidebar -->

                <!-- Right Sidebar -->
                <div class="col-lg-9">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="btn-toolbar" role="toolbar">
                        <div class="btn-group">
                          <button type="button" class="btn btn-common"><i class="lni-package"></i></button>
                          <button type="button" class="btn btn-common"><i class="lni-information"></i></button>
                          <button type="button" class="btn btn-common"><i class="lni-trash"></i></button>
                        </div>
                        <div class="btn-group ml-1">
                          <button type="button" class="btn btn-common dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="lni-folder"></i>
                            <b class="caret"></b>
                          </button>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="#" class="dropdown-item">Action</a></li>
                            <li><a href="#" class="dropdown-item">Another action</a></li>
                            <li><a href="#" class="dropdown-item">Something else here</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="#" class="dropdown-item">Separated link</a></li>
                          </ul>
                        </div>
                        <div class="btn-group ml-1">
                          <button type="button" class="btn btn-common dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="lni-tag"></i>
                            <b class="caret"></b>
                          </button>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="#" class="dropdown-item">Action</a></li>
                            <li><a href="#" class="dropdown-item">Another action</a></li>
                            <li><a href="#" class="dropdown-item">Something else here</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a href="#" class="dropdown-item">Separated link</a></li>
                          </ul>
                        </div>
                        <div class="btn-group ml-1">
                          <button type="button" class="btn btn-common dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            More
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a href="#" class="dropdown-item">Dropdown link</a></li>
                            <li><a href="#" class="dropdown-item">Dropdown link</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div> <!-- End row -->

                  <div class="card bg-dark m-t-20">
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table table-hover mail-content">
                          <tbody>
                            <tr>
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox0" value="check">
                                  <label class="custom-control-label" for="checkbox0"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled"></i>
                              </td>
                              <td>
                                <a href="email-read.html">Google Inc</a>
                              </td>
                              <td>
                                <a href="email-read.html"><i class="lni-check-mark-circle text-info m-r-15"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>
                                <i class="lni-link"></i>
                              </td>
                              <td class="text-right">
                                07:23 AM
                              </td>
                            </tr>

                            <tr>
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox1" value="check">
                                  <label class="custom-control-label" for="checkbox1"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled text-warning"></i>
                              </td>
                              <td>
                                <a href="email-read.html">John Deo</a>
                              </td>
                              <td>
                                <a href="email-read.html"><i class="lni-check-mark-circle text-success m-r-15"></i>Hi Bro, How are you?</a>
                              </td>
                              <td>

                              </td>
                              <td class="text-right">
                                07:03 AM
                              </td>
                            </tr>

                            <tr class="active">
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox2" value="check">
                                  <label class="custom-control-label" for="checkbox2"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled"></i>
                              </td>
                              <td>
                                <a href="email-read.html">Manager</a>
                              </td>
                              <td>
                                <a href="email-read.html"><i class="lni-check-mark-circle text-purple m-r-15"></i>Dolor sit amet, consectetuer adipiscing</a>
                              </td>
                              <td>
                                <i class="lni-link"></i>
                              </td>
                              <td class="text-right">
                                03:00 AM
                              </td>
                            </tr>

                            <tr>
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox3" value="check">
                                  <label class="custom-control-label" for="checkbox3"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled text-warning"></i>
                              </td>
                              <td>
                                <a href="email-read.html">Manager</a>
                              </td>
                              <td>
                                <a href="email-read.html"><i class="lni-check-mark-circle text-warning m-r-15"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>
                                <i class="lni-link"></i>
                              </td>
                              <td class="text-right">
                                22 Feb
                              </td>
                            </tr>

                            <tr>
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox4" value="check">
                                  <label class="custom-control-label" for="checkbox4"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled text-warning"></i>
                              </td>
                              <td>
                                <a href="email-read.html">Facebook</a>
                              </td>
                              <td>
                                <a href="email-read.html">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>

                              </td>
                              <td class="text-right">
                                22 Feb
                              </td>
                            </tr>

                            <tr class="active">
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox5" value="check">
                                  <label class="custom-control-label" for="checkbox5"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled text-warning"></i>
                              </td>
                              <td>
                                <a href="email-read.html">Google Inc</a>
                              </td>
                              <td>
                                <a href="email-read.html">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>

                              </td>
                              <td class="text-right">
                                21 Feb
                              </td>
                            </tr>

                            <tr class="active">
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox6" value="check">
                                  <label class="custom-control-label" for="checkbox6"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled"></i>
                              </td>
                              <td>
                                <a href="email-read.html">Twitter Inc</a>
                              </td>
                              <td>
                                <a href="email-read.html"><i class="lni-check-mark-circle text-info m-r-15"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>
                                <i class="lni-link"></i>
                              </td>
                              <td class="text-right">
                                21 Feb
                              </td>
                            </tr>

                            <tr class="active">
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox7" value="check">
                                  <label class="custom-control-label" for="checkbox7"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled"></i>
                              </td>
                              <td>
                                <a href="email-read.html">Jonaly Smith</a>
                              </td>
                              <td>
                                <a href="email-read.html"><i class="lni-check-mark-circle text-pink m-r-15"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>
                                <i class="lni-link"></i>
                              </td>
                              <td class="text-right">
                                19 Feb
                              </td>
                            </tr>

                            <tr class="active">
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox8" value="check">
                                  <label class="custom-control-label" for="checkbox8"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled"></i>
                              </td>
                              <td>
                                <a href="email-read.html">Google Inc</a>
                              </td>
                              <td>
                                <a href="email-read.html">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>
                                <i class="lni-link"></i>
                              </td>
                              <td class="text-right">
                                19 Feb
                              </td>
                            </tr>

                            <tr>
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox9" value="check">
                                  <label class="custom-control-label" for="checkbox9"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled"></i>
                              </td>
                              <td>
                                <a href="email-read.html">Google Inc</a>
                              </td>
                              <td>
                                <a href="email-read.html"><i class="lni-check-mark-circle text-info m-r-15"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>
                                <i class="lni-link"></i>
                              </td>
                              <td class="text-right">
                                19 Feb
                              </td>
                            </tr>

                            <tr>
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox10" value="check">
                                  <label class="custom-control-label" for="checkbox10"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled text-warning"></i>
                              </td>
                              <td>
                                <a href="email-read.html">John Deo</a>
                              </td>
                              <td>
                                <a href="email-read.html"><i class="lni-check-mark-circle text-success m-r-15"></i>Hi Bro, How are you?</a>
                              </td>
                              <td>

                              </td>
                              <td class="text-right">
                                18 Feb
                              </td>
                            </tr>

                            <tr class="active">
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox11" value="check">
                                  <label class="custom-control-label" for="checkbox11"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled"></i>
                              </td>
                              <td>
                                <a href="email-read.html">Manager</a>
                              </td>
                              <td>
                                <a href="email-read.html"><i class="lni-check-mark-circle text-purple m-r-15"></i>Dolor sit amet, consectetuer adipiscing</a>
                              </td>
                              <td>
                                <i class="lni-link"></i>
                              </td>
                              <td class="text-right">
                                18 Feb
                              </td>
                            </tr>

                            <tr>
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox12" value="check">
                                  <label class="custom-control-label" for="checkbox12"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled text-warning"></i>
                              </td>
                              <td>
                                <a href="#">Manager</a>
                              </td>
                              <td>
                                <a href="#"><i class="lni-check-mark-circle text-warning m-r-15"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>
                                <i class="lni-link"></i>
                              </td>
                              <td class="text-right">
                                15 Feb
                              </td>
                            </tr>

                            <tr>
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox13" value="check">
                                  <label class="custom-control-label" for="checkbox13"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled text-warning"></i>
                              </td>
                              <td>
                                <a href="#">Facebook</a>
                              </td>
                              <td>
                                <a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>

                              </td>
                              <td class="text-right">
                                15 Feb
                              </td>
                            </tr>

                            <tr class="active">
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox14" value="check">
                                  <label class="custom-control-label" for="checkbox14"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled text-warning"></i>
                              </td>
                              <td>
                                <a href="#">Google Inc</a>
                              </td>
                              <td>
                                <a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>

                              </td>
                              <td class="text-right">
                                14 Feb
                              </td>
                            </tr>

                            <tr class="active">
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox15" value="check">
                                  <label class="custom-control-label" for="checkbox15"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled"></i>
                              </td>
                              <td>
                                <a href="#">Twitter Inc</a>
                              </td>
                              <td>
                                <a href="#"><i class="lni-check-mark-circle text-info m-r-15"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>
                                <i class="lni-link"></i>
                              </td>
                              <td class="text-right">
                                12 Feb
                              </td>
                            </tr>

                            <tr class="active">
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox16" value="check">
                                  <label class="custom-control-label" for="checkbox16"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled"></i>
                              </td>
                              <td>
                                <a href="#">Jonaly Smith</a>
                              </td>
                              <td>
                                <a href="#"><i class="lni-check-mark-circle text-pink m-r-15"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>
                                <i class="lni-link"></i>
                              </td>
                              <td class="text-right">
                                12 Feb
                              </td>
                            </tr>

                            <tr class="active">
                              <td class="mail-select">
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="checkbox17" value="check">
                                  <label class="custom-control-label" for="checkbox17"></label>
                                </div>
                              </td>
                              <td class="mail-rateing">
                                <i class="lni-star-filled"></i>
                              </td>
                              <td>
                                <a href="#">Google Inc</a>
                              </td>
                              <td>
                                <a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
                              </td>
                              <td>
                                <i class="lni-link"></i>
                              </td>
                              <td class="text-right">
                                10 Feb
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-7">
                          <p class="text-white showing">Showing 1 - 20 of 289</p>
                        </div>
                        <div class="col-5 m-t-10">
                          <div class="btn-group float-right">
                            <button type="button" class="btn btn-common btn-sm"><i class="lni-chevron-left"></i></button>
                            <button type="button" class="btn btn-common btn-sm"><i class="lni-chevron-right"></i></button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div><!-- End row -->
            </div> <!-- container -->

    ''';
}
