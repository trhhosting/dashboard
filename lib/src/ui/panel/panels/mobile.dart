String mobilePanel(num total, String name){
  return '''
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="info-box bg-primary">
                            <div class="icon-box">
                                <i class="lni-home"></i>
                            </div>
                            <div class="info-box-content">
                                <h4 class="number">1125</h4>
                                <p class="info-text">All Properties</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="info-box bg-success">
                            <div class="icon-box">
                                <i class="lni-tag"></i>
                            </div>
                            <div class="info-box-content">
                                <h4 class="number">351</h4>
                                <p class="info-text">For Sale</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="info-box bg-info">
                            <div class="icon-box">
                                <i class="lni-cart"></i>
                            </div>
                            <div class="info-box-content">
                                <h4 class="number">774</h4>
                                <p class="info-text">For Rent</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="info-box bg-purple">
                            <div class="icon-box">
                                <i class="lni-wallet"></i>
                            </div>
                            <div class="info-box-content">
                                <h4 class="number">\$${total}</h4>
                                <p class="info-text">Total Revenue</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-12 col-xs-12">
                        <div class="row">
                            <div class="col-12">
                <div class="properties">
                  <div class="card bg-dark">
                    <div class="card-header border-bottom">
                      <h4 class="card-title text-white">Buttons example</h4>
                    </div>
                      <div class="card-body">
                        <p class="text-white m-b-30">The Buttons extension for DataTables
                          provides a common set of options, API methods and styling to display
                          buttons on a page that will interact with a DataTable. The core library
                          provides the based framework upon which plug-ins can built.
                        </p>
                        <div class="table-responsive">
                          <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                              <thead>
                              <tr>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Office</th>
                                <th>Age</th>
                                <th>Start date</th>
                                <th>Salary</th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                <td>Tiger Nixon</td>
                                <td>System Architect</td>
                                <td>Edinburgh</td>
                                <td>61</td>
                                <td>2011/04/25</td>
                                <td>\$320,800</td>
                              </tr>
                              <tr>
                                <td>Garrett Winters</td>
                                <td>Accountant</td>
                                <td>Tokyo</td>
                                <td>63</td>
                                <td>2011/07/25</td>
                                <td>\$170,750</td>
                              </tr>
                              <tr>
                                <td>Ashton Cox</td>
                                <td>Junior Technical Author</td>
                                <td>San Francisco</td>
                                <td>66</td>
                                <td>2009/01/12</td>
                                <td>\$86,000</td>
                              </tr>
                              <tr>
                                <td>Cedric Kelly</td>
                                <td>Senior Javascript Developer</td>
                                <td>Edinburgh</td>
                                <td>22</td>
                                <td>2012/03/29</td>
                                <td>\$433,060</td>
                              </tr>
                              <tr>
                                <td>Airi Satou</td>
                                <td>Accountant</td>
                                <td>Tokyo</td>
                                <td>33</td>
                                <td>2008/11/28</td>
                                <td>\$162,700</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
            
                    <div class="col-12 col-lg-4 col-xs-12">
                        <div class="card bg-dark">
                          <h3 class="card-header text-white text-center">
                          ${name.toUpperCase()}
                          </h3>
                          <div class="card-body">
                          <form class="form">
                            <div class="form-row">
                              <div class="form-check col-md-12 m-b-20">
                                <div class="custom-control custom-radio radio custom-control-inline">
                                  <input type="radio" class="custom-control-input" name="gender" id="male1" checked="">
                                  <label class="custom-control-label text-white" for="male1">Male</label>
                                </div>
                                <div class="custom-control custom-radio radio custom-control-inline">
                                  <input type="radio" class="custom-control-input" name="gender" id="female1" checked="">
                                  <label class="custom-control-label text-white" for="female1">Female</label>
                                </div>
                              </div>
                              <div class="form-group col-lg-6 floating-label">
                                <input type="text" class="form-control" id="firstname1">
                                <label class="text-white" for="firstname1">Firstname</label>
                              </div>
                              <div class="form-group col-lg-6 floating-label">
                                <input type="text" class="form-control" id="lastname1">
                                <label class="text-white" for="lastname1">Lastname</label>
                              </div>
                              <div class="form-group col-lg-12 floating-label">
                                <input type="text" class="form-control" id="username1">
                                <label class="text-white" for="username1">Username</label>
                              </div>
                              <div class="form-group col-lg-12 floating-label">
                                <input type="text" class="form-control" id="email1">
                                <label class="text-white" for="email1">Email</label>
                              </div>
                              <div class="form-group col-lg-6 floating-label">
                                <input type="password" class="form-control" id="password1">
                                <label class="text-white" for="password1">Password</label>
                              </div>
                              <div class="form-group col-lg-6 floating-label">
                                <input type="password" class="form-control" id="confirmPassword1">
                                <label class="text-white" for="confirmPassword1">Confirm Password</label>
                              </div>
                              <div class="form-check col-md-12">
                                <div class="custom-control custom-checkbox m-b-20">
                                  <input type="checkbox" class="custom-control-input" id="weeklyUpdates">
                                  <label class="custom-control-label text-white" for="weeklyUpdates">Send me weekly updates</label>
                                </div>
                              </div>
                              <div class="form-group col-lg-12 text-righ">
                                <button type="submit" class="btn btn-common">Submit</button>
                              </div>
                            </div>
                          </form>
                          </div>
                          
                        </div>
                    </div>
                </div>

    ''';
}
