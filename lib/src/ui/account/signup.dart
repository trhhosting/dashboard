String signUp(){
  String signupView = '''
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-lg-5 col-md-12 col-xs-12">
                  <div class="card bg-dark">
                      <div class="card-header border-bottom text-center">
                        <img width="200vh" src="/static/icons/proxyips/logo/proxyIPs_logo_nav.svg"><img>
                        <br>
                          <h4 class="card-title text-white">Register</h4>
                      </div>
                      <div class="card-body">
                          <form class="form-horizontal m-t-20">
                              <div class="form-group">
                                  <input id="reg-email" class="form-control" type="email" required="" placeholder="Email">
                              </div>
                              <div class="form-group">
                                  <input id="reg-username" class="form-control" type="text" required="" placeholder="Username">
                              </div>
                              <div class="form-group">
                                  <input id="reg-password-00" class="form-control" type="password" required="" placeholder="Password">
                              </div>
                              <div class="form-group">
                                  <input id="reg-password-01" class="form-control" type="password" required="" placeholder="Password">
                              </div>
                              <div class="form-group">
                                  <div class="custom-control custom-checkbox">
                                      <input type="checkbox" class="custom-control-input" id="customCheck1">
                                      <label class="custom-control-label text-white" for="customCheck1">I accept <a
                                              href="#" class="text-muted">Terms and Conditions</a></label>
                                  </div>
                              </div>
                              <div class="form-group text-center m-t-20">
                                  <a onclick="register();" class="btn btn-common btn-block" >Register</a>
                              </div>
                              <div class="form-group m-t-10 mb-0">
                                  <div class="text-center">
                                      <a onclick="myClickEvent('login','have_account')" class="text-muted">Already have account?</a>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>

  ''';
  return signupView;
}
String signUpBtc(){
  String signupView = '''
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-lg-5 col-md-12 col-xs-12">
                  <div class="card bg-dark">
                      <div class="card-header border-bottom text-center">
                        <img width="200vh" src="/static/icons/proxyips/logo/proxyIPs_logo_nav.svg"><img>
                        <br>
                          <h4 class="card-title text-white">Register</h4>
                      </div>
                      <div class="card-body">
                          <form class="form-horizontal m-t-20">
                              <div class="form-group">
                                  <input id="reg-btc-address" class="form-control" type="text" required="" placeholder="Email">
                              </div>
                              <div class="form-group">
                                  <input id="reg-email" class="form-control" type="email" required="" placeholder="Email">
                              </div>
                              <div class="form-group">
                                  <input id="reg-username" class="form-control" type="text" required="" placeholder="Username">
                              </div>
                              <div class="form-group">
                                  <input id="reg-password-00" class="form-control" type="password" required="" placeholder="Password">
                              </div>
                              <div class="form-group">
                                  <input id="reg-password-01" class="form-control" type="password" required="" placeholder="Password">
                              </div>
                              <div class="form-group">
                                  <div class="custom-control custom-checkbox">
                                      <input type="checkbox" class="custom-control-input" id="customCheck1">
                                      <label class="custom-control-label text-white" for="customCheck1">I accept <a
                                              href="#" class="text-muted">Terms and Conditions</a></label>
                                  </div>
                              </div>
                              <div class="form-group text-center m-t-20">
                                  <a onclick="register();" class="btn btn-common btn-block" >Register</a>
                              </div>
                              <div class="form-group m-t-10 mb-0">
                                  <div class="text-center">
                                      <a onclick="myClickEvent('login','have_account')" class="text-muted">Already have account?</a>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>

  ''';
  return signupView;
}
