String passwordNav(){
  return '''
                            <div class="form-group">
                                <div class="float-right">
                                    <a onclick="myClickEvent('login','reset_password')" class="text-white"><i class="lni-lock"></i> Forgot
                                        your password?</a>
                                </div>
                                <div class="float-left">
                                    <a onclick="myClickEvent('login','create_account')" class="text-white"><i class="lni-user"></i> Create an
                                        account</a>
                                </div>
                            </div>

  ''';
}



String login(){
    return loginView;
}

String loginView = '''
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-12 col-xs-12">
                <div class="card bg-dark">
                    <div class="card-header border-bottom text-center">
                        <img width="200vh" src="/static/icons/proxyips/logo/proxyIPs_logo_nav.svg"><img>
                        <br>
                        <h4 class="card-title text-white">Sign In</h4>
                    </div>
                    <div class="card-body">
                        <form  class="form-horizontal m-t-20">
                            <div class="form-group">
                                <input id="username" class="form-control" type="text" required="" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <input id="password" class="form-control" type="password" required="" placeholder="Password">
                            </div>
                            <div class="form-group">
                            </div>
                            <div class="form-group text-center m-t-20">
                                <a onclick="login()" class="btn btn-common btn-block">Log In</a>
                            </div>
                            ${passwordNav()}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

''';
String passwordReset(){
  

  return passwordResetView;
}

String passwordResetView = '''
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-12 col-xs-12">
                <div class="card bg-dark">
                    <div class="card-header border-bottom text-center">
                        <img width="200vh" src="/static/icons/proxyips/logo/proxyIPs_logo_nav.svg"><img>
                        <br>
                        <h4 class="card-title text-white">Sign In</h4>
                    </div>
                    <div class="card-body">
                        <form  class="form-horizontal m-t-20">
                            <div class="form-group">
                                <input id="email" class="form-control" type="text" required="" placeholder="Email">
                            </div>
                            <div class="form-group">
                            </div>
                            <div class="form-group text-center m-t-20">
                                <button onclick="passwordReset();" class="btn btn-common btn-block">Reset Password</button>
                            </div>
                          ${passwordNav()}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

''';