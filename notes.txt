<li id="mobile-proxies" class="nav-item dropdown">
    <a onclick="myClickEvent('MobileProxies','clicked')" class="dropdown-toggle">
        <span class="icon-holder">
            <i class="lni-laptop-phone"></i>
        </span>
        <span class="title">Mobile Proxies</span>
        <span class="arrow">
            <i class="lni-chevron-right"></i>
        </span>
    </a>
    <ul class="dropdown-menu sub-down">
        <li class="active">
            <a onclick="myClickEvent('MobileProxies','overview')">Overview</a>
        </li>
        <li>
            <a onclick="myClickEvent('MobileProxies','shared')">Shared</a>
        </li>
        <li>
            <a onclick="myClickEvent('MobileProxies','dedicated')">Dedicated</a>
        </li>
        <li>
            <a onclick="myClickEvent('MobileProxies','semi-dedicated')">Semi-Dedicated</a>
        </li>
        <li>
            <a onclick="myClickEvent('MobileProxies','sms')">Text Messaging</a>
        </li>
    </ul>
</li>
<li id="residential-proxies" class="nav-item dropdown">
    <a onclick="myClickEvent('ResidentialProxy','clicked')" class="dropdown-toggle">
        <span class="icon-holder">
            <i class="lni-world"></i>
        </span>
        <span class="title">Residential Proxy</span>
        <span class="arrow">
            <i class="lni-chevron-right"></i>
        </span>
    </a>
    <ul class="dropdown-menu sub-down">
        <li class="active">
            <a onclick="myClickEvent('ResidentialProxy','overview')">Overview</a>
        </li>
        <li>
            <a onclick="myClickEvent('ResidentialProxy','bandwidth')">Bandwidth</a>
        </li>
        <li>
            <a onclick="myClickEvent('ResidentialProxy','rotation')">Rotation</a>
        </li>
    </ul>
</li>
<li id="datacenter-proxies" class="nav-item dropdown">
    <a onclick="myClickEvent('ResidentialProxy','clicked')" class="dropdown-toggle">
        <span class="icon-holder">
            <i class="lni-cloud-sync"></i>
        </span>
        <span class="title">Datacenter Proxy</span>
        <span class="arrow">
            <i class="lni-chevron-right"></i>
        </span>
    </a>
    <ul class="dropdown-menu sub-down">
        <li class="active">
            <a onclick="myClickEvent('DataCenterProxy','overview')">Overview</a>
        </li>
        <li>
            <a onclick="myClickEvent('DataCenterProxy','bandwidth')">Bandwidth</a>
        </li>
        <li>
            <a onclick="myClickEvent('DataCenterProxy','rotation')">Rotation</a>
        </li>
    </ul>
</li>
<li id="account" class="nav-item dropdown">
    <a onclick="myClickEvent('Account','clicked')" class="dropdown-toggle">
        <span class="icon-holder">
            <i class="lni-cloud-sync"></i>
        </span>
        <span class="title">Account</span>
        <span class="arrow">
            <i class="lni-chevron-right"></i>
        </span>
    </a>
    <ul class="dropdown-menu sub-down">
        <li class="active">
            <a onclick="myClickEvent('Account','overview')">Overview</a>
        </li>
        <li>
            <a onclick="myClickEvent('Account','addons')">AddOn</a>
        </li>
        <li>
            <a onclick="myClickEvent('Account','billing')">Billing</a>
        </li>
    </ul>
</li>
