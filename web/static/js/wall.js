function dispatchAddCartEvent(productName, productID, price, priceID, usageType) {
    var event = new CustomEvent("addCart", {
      detail: {
        "name": productName,
        "price": price,
        "product_id": productID,
        "price_id": priceID,
        "usage_type": usageType,

      }
    });

    document.dispatchEvent(event);
    alert(productName +" has been added to your Cart.");
}
function dispatchDelCartEvent(productName, productID, price, priceID, usageType) {
    var event = new CustomEvent("delCart", {
      detail: {
        "name": productName,
        "price": price,
        "product_id": productID,
        "price_id": priceID,
        "usage_type": usageType,
      }
    });
    document.dispatchEvent(event);
    alert(productName +" has been removed to your Cart.");
}
function checkoutEvent(type, price) {
    var event = new CustomEvent("checkoutCart", {
      detail: {
        "payment_type": type,
        "price": price,
      }
    });
    document.dispatchEvent(event);
}
function loginEvent(user_name, password) {
    var event = new CustomEvent("login", {
      detail: {
        "user_name": user_name,
        "password": password,
      }
    });
    document.dispatchEvent(event);
}
function myClickEvent(name, action) {
    var event = new CustomEvent("myClick", {
      detail: {
        "name": name,
        "action": action,
      }
    });
    document.dispatchEvent(event);
}
function registerEvent(email, btcAddress, passwordOne, passwordTwo, userName) {
    var event = new CustomEvent("register", {
      detail: {
        "email": email,
        "btc_address": btcAddress,
        "password_one": passwordOne,
        "password_two": passwordTwo,
        "user_name": userName,
      }
    });
    document.dispatchEvent(event);
}