import 'dart:async';
import 'dart:html';
import 'dart:convert';
import 'package:apt/src/ui/model/model.dart';
import 'package:apt/src/ui/ui.dart';
import 'package:apt/insert.dart';

var gcxval = CustomHtmlValidator();
const gcxSec = Duration(seconds:1);
String panelSelect = "notification";
void main() {
  // Login
  DivElement loginDiv = querySelector('#login');
  DivElement signup = querySelector('#signup');
  DivElement resetPassword = querySelector('#reset-password');
  // SideNav
  DivElement sideNav = querySelector('#side-nav');
  LIElement ingress = querySelector('#ingress');
  LIElement mobileProxies = querySelector('#mobile-proxies');
  LIElement residentialProxies = querySelector('#residential-proxies');
  LIElement datacenterProxies = querySelector('#datacenter-proxies');
  LIElement account = querySelector('#account');

  // Messages
  SpanElement mailCounter = querySelector('#mail-counter');
  SpanElement notificationCounter = querySelector('#notification-counter');
  loginDiv.appendHtml(
    login(),
    validator: gcxval.validator,
    );
  signup.appendHtml(
    signUp(),
    validator: gcxval.validator,
    );
  
  resetPassword.appendHtml(
    passwordReset(),
    validator: gcxval.validator,
    );
  //Dashboard
  //Metered display
  DivElement dashboard = querySelector('#dashboard');
  dashboard.appendHtml(
    ingressPanel(0, 'ingress'),
    validator: gcxval.validator,
  );  
  void selectPanel(num panelTick, String panelName){
        panelSelect = panelName;
        dashboard.nodes.clear();
        var dash = dashboardUpdate(panelTick, panelSelect);
        dashboard.replaceWith(dash);
        dashboard = dash;

  }

    // System messages Display
    DivElement sysMessage = querySelector('#sys-message');
    var msg = sysMessage.children.first;
    msg.innerText = "System is working";
  // Click Message listener
  document.on["myClick"].listen((Event event) {
    var data = (event as CustomEvent).detail;
    String dat = json.encode(data);
    Map<String, dynamic> dataConvert = json.decode(dat);
    print(dat);
    MyEvent myEvent = MyEvent.fromJson(dataConvert);
    switch (myEvent.name){
      case "login":
        switch (myEvent.action){
          case "create_account":
            loginDiv.hidden = true;
            signup.hidden = false;
            resetPassword.hidden = true;
          break;
          case "have_account":
            signup.hidden = true;
            loginDiv.hidden = false;
            resetPassword.hidden = true;
          break;
          case "reset_password":
            signup.hidden =true;
            loginDiv.hidden = true;
            resetPassword.hidden = false;
          break;
          case "password":
            signup.hidden =true;
            loginDiv.hidden = true;
            resetPassword.hidden = false;
          break;
          default:
          break;
        }
        break;
      case "logo":
        if(myEvent.action == "clicked"){
          bool x = sideNav.hidden;
          if(x){
            sideNav.hidden = false;
          }
          if(!x){
            sideNav.hidden = true;
          }
        }
      break;
      case "Ingress":
        if(myEvent.action == "clicked"){
          String x = ingress.getAttribute("class");
          List<String> cx = x.split(" ");
          if(cx.contains("open")){
            ingress.setAttribute("class", "nav-item dropdown");
          }
          if(!cx.contains("open")){
            ingress.setAttribute("class", "nav-item dropdown open");
          }
          panelSelect = "ingress";
        }
      break;
      case "MobileProxies":
        if(myEvent.action == "clicked"){
          String x = mobileProxies.getAttribute("class");
          List<String> cx = x.split(" ");
          if(cx.contains("open")){
            mobileProxies.setAttribute("class", "nav-item dropdown");
          }
          if(!cx.contains("open")){
            mobileProxies.setAttribute("class", "nav-item dropdown open");
          }
          panelSelect = "mobile";
          
        }
      break;
      case "ResidentialProxies":
        if(myEvent.action == "clicked"){
          String x = residentialProxies.getAttribute("class");
          List<String> cx = x.split(" ");
          if(cx.contains("open")){
            residentialProxies.setAttribute("class", "nav-item dropdown");
          }
          if(!cx.contains("open")){
            residentialProxies.setAttribute("class", "nav-item dropdown open");
          }
          panelSelect = "residential";
        }
      break;
      case "DataCenterProxies":
        if(myEvent.action == "clicked"){
          String x = datacenterProxies.getAttribute("class");
          List<String> cx = x.split(" ");
          if(cx.contains("open")){
            datacenterProxies.setAttribute("class", "nav-item dropdown");
          }
          if(!cx.contains("open")){
            datacenterProxies.setAttribute("class", "nav-item dropdown open");
          }
          panelSelect = "datacenter";
        }
      break;
      case "Account":
        if(myEvent.action == "clicked"){
          String x = account.getAttribute("class");
          List<String> cx = x.split(" ");
          if(cx.contains("open")){
            account.setAttribute("class", "nav-item dropdown");
          }
          if(!cx.contains("open")){
            account.setAttribute("class", "nav-item dropdown open");
          }
          panelSelect = "account";
        }
      break;
      case "Mail":
        if(myEvent.action == "clicked"){
            // TODO: add mail api 
            mailCounter.hidden = true;
            mailCounter.innerText = "30";
          panelSelect = "mail";
        }
      break;
      case "Notification":
        if(myEvent.action == "clicked"){
          // TODO: add notification api
            notificationCounter.hidden = true;
            notificationCounter.innerText = "30";
            panelSelect = 'notification';
        }
      break;
    }
   
  });
 document.on["addCart"].listen((Event event) {
    var data = (event as CustomEvent).detail;
    var dataConvert = json.encode(data);
    print(dataConvert);
   
  });
  document.on["delCart"].listen((Event event) {
    var data = (event as CustomEvent).detail;
    var dataConvert = json.encode(data);
    print(dataConvert);
  });

  DivElement panel = querySelector('#panel');
  document.on["login"].listen((Event event) {
    var data = (event as CustomEvent).detail;
    var dataConvert = json.encode(data);
    UserLogin userLogin = UserLogin.fromJson(json.decode(dataConvert));
    if (userLogin.password == "password"){
            signup.hidden =true;
            loginDiv.hidden = true;
            resetPassword.hidden = true;
            panel.hidden = false;

    }
    print(dataConvert);

  });
  document.on["register"].listen((Event event) {
    var data = (event as CustomEvent).detail;
    var dataConvert = json.encode(data);
    print(dataConvert);
  });
  Timer.periodic(gcxSec, (t){
    print(t.tick);
    dashboard.nodes.clear();
    if(dashboard.nodes.isEmpty || dashboard.nodes == null){
      selectPanel(t.tick, panelSelect);
    }
  });
}
void createAccount(){
  var login = querySelector('#login');
  login.hidden = false;

  var signup = querySelector('#signup');
  signup.hidden = true;

}

DivElement dashboardUpdate(num data, String type){
    DivElement dash = blankDashboard();
    switch (type){
      case "ingress":
        dash.appendHtml(
            ingressPanel(data, type),
            validator: gcxval.validator,
        );
      break;
      case "mobile":
        dash.appendHtml(
            mobilePanel(data, type),
            validator: gcxval.validator,
        );
      break;
      case "residential":
        dash.appendHtml(
            residentialPanel(data, type),
            validator: gcxval.validator,
        );
      break;
      case "datacenter":
        dash.appendHtml(
            datacenterPanel(data, type),
            validator: gcxval.validator,
        );
      break;
      case "account":
        dash.appendHtml(
            accountPanel(data, type),
            validator: gcxval.validator,
        );
      break;
      case "mail":
        dash.appendHtml(
            mailPanel(data, type),
            validator: gcxval.validator,
        );
      break;
      case "notification":
        dash.appendHtml(
            notificationPanel(data, type),
            validator: gcxval.validator,
        );
      break;
    }
  return dash;
}